/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.atg.zui.astar.jencidus.astar;

import eu.superhub.wp5.planner.planningstructure.GraphEdge;
import eu.superhub.wp5.planner.planningstructure.GraphNode;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dusanjencik
 */
public class Path {

    private ArrayList<Node> waypoints = new ArrayList<Node>();

    public int getLength() {
        return waypoints.size();
    }

    public GraphNode getWayPoint(int index) {
        return waypoints.get(index);
    }

    public List<GraphEdge> getListOfGraphEdges() {
        List<GraphEdge> list = new ArrayList<>(waypoints.size());
        for (Node graphNode : waypoints) {
            list.add(graphNode.getEdgeToNode());
        }
        return list;
    }

    /**
     * Append a waypoint to the path.
     */
    public void appendWayPoint(Node n) {
        waypoints.add(n);
    }

    /**
     * Add a waypoint to the beginning of the path.
     */
    public void prependWayPoint(Node n) {
        waypoints.add(0, n);
    }

    /**
     * Check if this path contains the WayPoint
     *
     * @param id The id of the waypoint.
     * @return True if the path contains the waypoint.
     */
    public boolean contains(long id) {
        for (GraphNode node : waypoints) {
            if (node.getId() == id) {
                return true;
            }
        }
        return false;
    }
}
