/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.atg.zui.astar.jencidus.astar;

import eu.superhub.wp5.planner.planningstructure.GraphEdge;
import eu.superhub.wp5.planner.planningstructure.GraphNode;
import eu.superhub.wp5.wp5common.GPSLocation;
import java.util.Set;

/**
 *
 * @author dusanjencik
 */
public class Node extends GraphNode implements Comparable<Node> {

    private double timeFromStart;
    private double heuristicTimeFromGoal;
    private Node previousNode;
    private GraphEdge edgeToNode;

    public Node(long id, GPSLocation gpsLocation, String description, Set<String> gtfsStopIDs) {
        super(id, gpsLocation, description, gtfsStopIDs);
        timeFromStart = Double.MAX_VALUE;
    }

    public Node(GraphNode node) {
        super(node.getId(), node.getGpsLocation(), node.getDescription(), node.getGtfsStopIDs());
        timeFromStart = Double.MAX_VALUE;
    }

    public GraphEdge getEdgeToNode() {
        return edgeToNode;
    }

    public void setEdgeToNode(GraphEdge edgeToNode) {
        this.edgeToNode = edgeToNode;
    }

    public double getTimeFromStart() {
        return timeFromStart;
    }

    public void setTimeFromStart(double timeFromStart) {
        this.timeFromStart = timeFromStart;
    }

    public double getHeuristicTimeFromGoal() {
        return heuristicTimeFromGoal;
    }

    public void setHeuristicTimeFromGoal(double heuristicTimeFromGoal) {
        this.heuristicTimeFromGoal = heuristicTimeFromGoal;
    }

    public Node getPreviousNode() {
        return previousNode;
    }

    public void setPreviousNode(Node previousNode) {
        this.previousNode = previousNode;
    }

    @Override
    public int compareTo(Node otherNode) {
        double thisTotalTimeFromGoal = heuristicTimeFromGoal + timeFromStart;
        double otherTotalTimeFromGoal = otherNode.getHeuristicTimeFromGoal() + otherNode.getTimeFromStart();

        if (thisTotalTimeFromGoal < otherTotalTimeFromGoal) {
            return -1;
        } else if (thisTotalTimeFromGoal > otherTotalTimeFromGoal) {
            return 1;
        } else {
            return 0;
        }
    }
}
