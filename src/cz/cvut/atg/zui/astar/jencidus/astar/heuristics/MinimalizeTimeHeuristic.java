/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.atg.zui.astar.jencidus.astar.heuristics;

import cz.cvut.atg.zui.astar.jencidus.astar.Node;
import eu.superhub.wp5.planner.planningstructure.GraphNode;
import org.openstreetmap.osm.data.coordinates.LatLon;

/**
 *
 * @author dusanjencik
 */
public class MinimalizeTimeHeuristic implements AStarHeuristic {

    private double latGoal, lonGoal;

    public MinimalizeTimeHeuristic(GraphNode node) {
        latGoal = node.getLatitude();
        lonGoal = node.getLongitude();
    }

    @Override
    public double getEstimatedCostToGoal(Node current) {
        double dist = LatLon.distanceInMeters(current.getLatitude(), current.getLongitude(), latGoal, lonGoal) / 1000.;
        return dist / 120.; // min 7.5, max 120 kmph
    }
}
