/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.atg.zui.astar.jencidus;

import cz.cvut.atg.zui.astar.AbstractOpenList;
import cz.cvut.atg.zui.astar.PlannerInterface;
import cz.cvut.atg.zui.astar.RoadGraph;
import cz.cvut.atg.zui.astar.jencidus.astar.AStar;
import cz.cvut.atg.zui.astar.jencidus.astar.Path;
import eu.superhub.wp5.planner.planningstructure.GraphEdge;
import eu.superhub.wp5.planner.planningstructure.GraphNode;
import java.util.List;

/**
 *
 * @author dusanjencik
 */
public class Planner implements PlannerInterface<GraphNode> {

    private AStar pathFinder;

    @Override
    public List<GraphEdge> plan(RoadGraph graph, GraphNode origin, GraphNode destination) {
        pathFinder = new AStar(graph, destination);
        Path path = pathFinder.calcShortestPath(origin, destination);

        if (path == null) {
            return null;
        }

        return path.getListOfGraphEdges();
    }

    @Override
    public AbstractOpenList<GraphNode> getOpenList() {
        return pathFinder.getOpenList();
    }
}
