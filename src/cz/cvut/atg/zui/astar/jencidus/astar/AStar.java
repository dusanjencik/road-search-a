/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.atg.zui.astar.jencidus.astar;

import cz.cvut.atg.zui.astar.RoadGraph;
import cz.cvut.atg.zui.astar.jencidus.astar.heuristics.AStarHeuristic;
import cz.cvut.atg.zui.astar.jencidus.astar.heuristics.MinimalizeTimeHeuristic;
import eu.superhub.wp5.planner.planningstructure.GraphEdge;
import eu.superhub.wp5.planner.planningstructure.GraphNode;
import eu.superhub.wp5.planner.planningstructure.PermittedMode;
import java.util.HashSet;
import java.util.List;

/**
 *
 * @author dusanjencik
 */
public class AStar {

    private RoadGraph map;
    private AStarHeuristic heuristicTime;
    /**
     * closedList The list of Nodes not searched yet, sorted by their distance
     * to the goal as guessed by our heuristic.
     */
    private HashSet<Node> closedList;
    private OpenList openList;
    private Node start, goal;

    public AStar(RoadGraph map, GraphNode destination) {
        this.map = map;
        this.heuristicTime = new MinimalizeTimeHeuristic(destination);

        closedList = new HashSet<>();
        openList = new OpenList();
    }

    public OpenList getOpenList() {
        return openList;
    }

    public Path calcShortestPath(GraphNode startNode, GraphNode goalNode) {
        //mark start and goal node
        this.start = new Node(startNode);
        this.goal = new Node(goalNode);

        //Check if the goal node is blocked (if it is, it is impossible to find a path there)
        if (map.getNodeOutcomingEdges(start.getId()).isEmpty()
                || map.getNodeIncomingEdges(goal.getId()).isEmpty()) {
            return null;
        }

        start.setTimeFromStart(0);
        closedList.clear();
        openList.clear();
        start.setHeuristicTimeFromGoal(heuristicTime.getEstimatedCostToGoal(start));
        openList.add(start);

        //while we haven't reached the goal yet
        while (openList.size() != 0) {

            //get the first Node from non-searched Node list, sorted by lowest distance from our goal as guessed by our heuristic
            Node current = openList.getFirst();

            // check if our current Node location is the goal Node. If it is, we are done.
            if (current.getId() == goal.getId()) {
                return reconstructPath(current);
            }

            //move current Node to the closed (already searched) list
            openList.remove(current);
            closedList.add(current);


            //go through all the current Nodes neighbors and calculate if one should be our next step
            List<GraphEdge> ways = map.getNodeOutcomingEdges(current.getId());
            if (ways != null) {
                for (GraphEdge neighborEdge : ways) {
                    if (!neighborEdge.getPermittedModes().contains(PermittedMode.CAR)) {
                        continue;
                    }
                    boolean contains;
                    Node neighborNode = new Node(map.getNodeByNodeId(neighborEdge.getToNodeId()));

                    //if we have already searched this Node, don't bother and continue to the next one 
                    if (closedList.contains(neighborNode)) {
                        continue;
                    }

                    // calculate how long the path is if we choose this neighbor as the next step in the path 
                    double neighborTimeFromStart = current.getTimeFromStart()
                            + ((neighborEdge.getLengthInMetres() / 1000.) / neighborEdge.getAllowedMaxSpeedInKmph());

                    contains = openList.contains(neighborNode);
                    if (!contains || neighborTimeFromStart < neighborNode.getTimeFromStart()) {
                        neighborNode.setPreviousNode(current);
                        neighborNode.setTimeFromStart(neighborTimeFromStart);
                        neighborNode.setEdgeToNode(neighborEdge);
                        neighborNode.setHeuristicTimeFromGoal(heuristicTime.getEstimatedCostToGoal(neighborNode));

                        if (contains) {
                            openList.update(neighborNode);
                        } else {
                            openList.add(neighborNode);
                        }
                    }
                }
            }
        }
        return null;
    }

    private Path reconstructPath(Node node) {
        Path path = new Path();
        while (!(node.getPreviousNode() == null)) {
            path.prependWayPoint(node);
            node = node.getPreviousNode();
        }
        return path;
    }
}
