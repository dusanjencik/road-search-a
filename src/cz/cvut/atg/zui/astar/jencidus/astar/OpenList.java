/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.atg.zui.astar.jencidus.astar;

import cz.cvut.atg.zui.astar.AbstractOpenList;
import java.util.HashSet;
import java.util.PriorityQueue;

/**
 *
 * @author dusanjencik
 */
public class OpenList extends AbstractOpenList {

    private PriorityQueue<Node> list = new PriorityQueue<>();
    private HashSet<Long> set = new HashSet<>();

    public Node getFirst() {
        return list.poll();
    }

    public void clear() {
        list.clear();
        set.clear();
    }

    public void update(Node n) {
        list.add(n);
    }
    
    public void remove(Node n) {
        list.remove(n);
        set.remove(n.getId());
    }

    public int size() {
        return list.size();
    }

    public boolean contains(Node n) {
        return set.contains(n.getId());
    }

    @Override
    protected boolean addItem(Object item) {
        Node n = (Node) item;
        list.add(n);
        set.add(n.getId());
        return true;
    }
}
