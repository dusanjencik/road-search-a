# Path Planning A* #

School homework.

## Assignment ##

### Input: ###
* Graph representing road network in UK
* The graph edges contain length of the edge and maximal allowed speed
* The nodes are signed with unique identificator ID.
* Initial node
* Goal node

### Output: ###
* A path between the initial and the goal node

### Quality criteria, listed with descending priority: ###
* The algorihtm fulfils a* ll parts of the assignment (classes name and location, using given structures and the method add() in the open list - see below)
* The path is correct, i.e., leads from origin to destination and the path is connected, i.e., two subsequent edges have a common node.
* The found path minimises transport time between initial and goal state
* The number of expanded nodes is minimal
