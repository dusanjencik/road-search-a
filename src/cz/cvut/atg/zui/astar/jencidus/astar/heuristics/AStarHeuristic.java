/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.atg.zui.astar.jencidus.astar.heuristics;

import cz.cvut.atg.zui.astar.jencidus.astar.Node;

/**
 *
 * @author dusanjencik
 */
public interface AStarHeuristic {

        /**
         * 
         * The heuristic tries to guess how far a given Node is from the goal Node.
         * The lower the cost, the more likely a Node will be searched next. 
         * 
         * @return The cost associated with the given tile
         */
        public double getEstimatedCostToGoal(Node current);
}
